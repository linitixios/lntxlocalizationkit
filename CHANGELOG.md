# LNTXLocalizationKit CHANGELOG

## 0.4.1

- **Added** `UISearchBar+LNTXLocalization`: Category for associating localization keys to update a `UISearchBar`'s text and placeholder.

## 0.4.0

**Added** `IBInspectable` attribute on localisation properties to ease their edition in IB.

## 0.3.1

**Fixed** `LNTXLocalizationManager`: Now returns supported languages (bundles) sorted by preferred languages.

## 0.3.0

**Added** A property `allowedLanguages` to limit the number of supported languages at runtime.

## 0.2.1

**Fixed** Localization macros by removining trailing semicolon.

## 0.2.0
- **Added** `UIBarItem+LNTXLocalization`: Category for associating a localization key to update a `UIBarItem`'s title.
- **Added** `UINavigationItem+LNTXLocalization`: Category for associating a localization key to update a `UINavigationItem`'s title.
- **Added** `UISegmentedControl+LNTXLocalization`: Category for associating a localization key to update a `UISegmentedControl`'s segment titles.
- **Added** `UITextField+LNTXLocalization`: Category for associating localization keys to update a `UITextField`'s text and placeholder.
- **Added** `UITextView+LNTXLocalization`: Category for associating a localization key to update a `UITextView`'s text.
- **Added** `UIViewController+LNTXLocalization`: Category for associating a localization key to update a `UIViewController`'s title.

## 0.1.0

Initial release.

- **Added** `LNTXLocalizationManager`: Basic methods for updating the application language and retrieving localized string values for the current application language.
- **Added** `NSObject+LNTXLocalization`: Category for associating localization keys for specifick keypaths on any `NSObject` object.
- **Added** `UILabel+LNTXLocalization`: Category for associating a localization key for the `UILabel` `text` property.
- **Added** `UIButton+LNTXLocalization`: Category for associating a localization key to update a `UIButton`'s title for all its possible states.