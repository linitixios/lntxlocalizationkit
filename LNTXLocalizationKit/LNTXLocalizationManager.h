//
//  LNTXLocalizationManager.h
//  LNTXLocalizationKit
//
//  Created by Damien Rambout on 04/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import <UIKit/UIKit.h>

// Quickly updates the current application language.
#define LNTXSetCurrentLanguage(languageISOCode) \
    [[LNTXLocalizationManager sharedManager] setCurrentLanguage:languageISOCode]
// Quickly retrieves the current application language.
#define LNTXGetCurrentLanguage() \
    [[LNTXLocalizationManager sharedManager] currentLanguage]

// Quickly retrieve a localized string.
#define LNTXLocalizedString(key) [[LNTXLocalizationManager sharedManager] localizedStringForKey:(key)]
// Quickly retrieve a localized string from a localization table.
#define LNTXLocalizedStringFromTable(key, tbl) [[LNTXLocalizationManager sharedManager] localizedStringForKey:(key) fromTable:(tbl)]

/** Posted immediately prior to a language update. */
extern NSString * const LNTXLanguageWillUpdateNotification;
/** Posted immediately after a language update. */
extern NSString * const LNTXLanguageDidUpdateNotification;
/**
 The key for a NSString object that identifies the language prior the update. 
 This language is a ISO language code.
 */
extern NSString * const LNTXPreviousLanguageUserInfoKey;
/**
 The key for a NSString object that identifies the current language.
 This language is a ISO language code.
 */
extern NSString * const LNTXCurrentLanguageUserInfoKey;

/**
 @brief Helper class used to update the application language at runtime and retrieve 
 localized value based on this language.
 */
@interface LNTXLocalizationManager : NSObject

/** 
 brief Returns a singleton instance of this manager.
 @return A singleton instance of this manager.
 */
+ (instancetype)sharedManager;

/**
 @brief Returns the list of supported languages for this application. 
 This list is basically all the languages (bundles) defined in the project.
 @return The list of supported languages for this application.
 */
+ (NSArray *)supportedLanguages;
/**
 @brief Returns the preferred language for the running application.
 This language is based on the first intersection between the device preferred languages 
 and the application supported languages.
 @return The preferred language for the running application.
 */
+ (NSString *)preferredLanguage;

/** @brief The current language ISO code. */
@property (nonatomic, copy) NSString *currentLanguage;
/** @brief The bundle associated with the current language. */
@property (nonatomic, readonly) NSBundle *currentBundle;
/** @brief A set of allowed language ISO codes. Must be a subset of `+supportedLanguages`. */
@property (nonatomic, copy) NSSet *allowedLanguages;

/**
 @brief Returns a localized string in the current language for a specific key. This key must be
 located in the default localization table.
 If the key does not exist for the current language, then the key will be returned instead of a 
 localized value.
 @return A localized string in the current language for a specific key.
 */
- (NSString *)localizedStringForKey:(NSString *)key;
/**
 @brief Returns a localized string in the current language for a specific key, in a specific 
 localization table.
 If the key does not exist for the current language, then the key will be returned instead of a
 localized value.
 @return A localized string in the current language for a specific key, in a specific
 localization table.
 */
- (NSString *)localizedStringForKey:(NSString *)key fromTable:(NSString *)table;

/**
 @brief Returns the full localized pathname for the resource identified by the specified name and file extension. 
 This method will search in the current language's bundle if the
 resource can be found. If no resource could be found, the method will search in the main bundle.
 @return The full localized pathname for the resource file or nil if the file could not be located.
 */
- (NSString *)localizedPathForResource:(NSString *)name ofType:(NSString *)extension;

@end
