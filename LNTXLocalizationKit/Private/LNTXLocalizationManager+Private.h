//
//  LNTXLocalizationManager+Private.h
//  LNTXLocalizationKit
//
//  Created by Damien Rambout on 05/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import <LNTXLocalizationKit/LNTXLocalizationManager.h>

extern NSString * const LNTXInternalLanguageDidUpdateNotification;

@interface LNTXLocalizationManager (Private)

@end
