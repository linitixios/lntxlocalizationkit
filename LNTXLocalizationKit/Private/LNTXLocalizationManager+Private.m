//
//  LNTXLocalizationManager+Private.m
//  LNTXLocalizationKit
//
//  Created by Damien Rambout on 05/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import "LNTXLocalizationManager+Private.h"

NSString * const LNTXInternalLanguageDidUpdateNotification = @"LNTXInternalLanguageDidUpdateNotification";

@implementation LNTXLocalizationManager (Private)

@end
