//
//  UISegmentedControl+LNTXLocalization.m
//  LNTXLocalizationKit
//
//  Created by Damien Rambout on 17/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import "UISegmentedControl+LNTXLocalization.h"
#import "LNTXLocalizationManager.h"

#import <objc/runtime.h>

static NSString * const kLocalizedTitleKeyPath = @"lntx_localizedTitle";

@interface UISegmentedControl ()

@property (nonatomic) NSString *lntx_localizedTitle;

@end

@implementation UISegmentedControl (LNTXLocalization)

#pragma mark - Public accessors

- (NSString *)localizationKeyPrefix {
    return [self localizationKeyForKeyPath:kLocalizedTitleKeyPath];
}

- (void)setLocalizationKeyPrefix:(NSString *)localizationKeyPrefix {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        // Switch methods to update localized content when required
        [self swizzleOriginalMethod:@selector(insertSegmentWithImage:atIndex:animated:)
                   withCustomMethod:@selector(lntx_insertSegmentWithImage:atIndex:animated:)];
        [self swizzleOriginalMethod:@selector(insertSegmentWithTitle:atIndex:animated:)
                   withCustomMethod:@selector(lntx_insertSegmentWithTitle:atIndex:animated:)];
    });
    
    [self setLocalizationKey:localizationKeyPrefix forKeyPath:kLocalizedTitleKeyPath];
}

#pragma mark - Private accessors

- (void)setLntx_localizedTitle:(NSString *)lntx_localizedTitle {
    // Update all segments
    [self reloadLocalizedTitles];
}

- (void)reloadLocalizedTitles {
    for (NSUInteger i = 0; i < self.numberOfSegments; i++) {
        NSString *localizationKey = [self titleLocalizationKeyForSegmentAtIndex:i];
        if (localizationKey) {
            NSString *localizedValue = LNTXLocalizedStringFromTable(localizationKey,
                                                                    self.localizationTable);
            [self setTitle:localizedValue forSegmentAtIndex:i];
        }
    }
}

#pragma mark - Method swizzling

- (void)swizzleOriginalMethod:(SEL)originalSel withCustomMethod:(SEL)customSel {
    Method original = class_getInstanceMethod(self.class, originalSel);
    Method swizzled = class_getInstanceMethod(self.class, customSel);
    method_exchangeImplementations(original, swizzled);
}

- (void)lntx_insertSegmentWithImage:(UIImage *)image atIndex:(NSUInteger)segment animated:(BOOL)animated {
    // Method should be swizzled, so this will call the original implementation
    [self lntx_insertSegmentWithImage:image atIndex:segment animated:animated];
    [self reloadLocalizedTitles];
}

- (void)lntx_insertSegmentWithTitle:(NSString *)title atIndex:(NSUInteger)segment animated:(BOOL)animated {
    // Method should be swizzled, so this will call the original implementation
    [self lntx_insertSegmentWithTitle:title atIndex:segment animated:animated];
    [self reloadLocalizedTitles];
}

#pragma mark - Helper methods

- (NSString *)titleLocalizationKeyForSegmentAtIndex:(NSUInteger)index {
    if (!self.localizationKeyPrefix) {
        return nil;
    }
    
    return [NSString stringWithFormat:@"%@%lu", self.localizationKeyPrefix, (unsigned long)index];
}

@end
