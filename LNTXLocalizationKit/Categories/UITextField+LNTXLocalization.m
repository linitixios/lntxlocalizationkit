//
//  UITextField+LNTXLocalization.m
//  LNTXLocalizationKit
//
//  Created by Damien Rambout on 17/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import "UITextField+LNTXLocalization.h"

static NSString * const kTextKeyPath = @"text";
static NSString * const kPlaceholderKeyPath = @"placeholder";

@implementation UITextField (LNTXLocalization)

#pragma mark - Public accessors

- (NSString *)localizationKey {
    return [self localizationKeyForKeyPath:kTextKeyPath];
}

- (void)setLocalizationKey:(NSString *)localizationKey {
    [self setLocalizationKey:localizationKey forKeyPath:kTextKeyPath];
}

- (NSString *)placeholderLocalizationKey {
    return [self localizationKeyForKeyPath:kPlaceholderKeyPath];
}

- (void)setPlaceholderLocalizationKey:(NSString *)placeholderLocalizationKey {
    [self setLocalizationKey:placeholderLocalizationKey forKeyPath:kPlaceholderKeyPath];
}

@end
