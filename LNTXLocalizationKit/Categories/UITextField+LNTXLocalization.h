//
//  UITextField+LNTXLocalization.h
//  LNTXLocalizationKit
//
//  Created by Damien Rambout on 17/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <LNTXLocalizationKit/NSObject+LNTXLocalization.h>

/**
 @brief Extension with properties and methods for localizing a UITextField object.
 */
@interface UITextField (LNTXLocalization)

#pragma mark - Title localization
/**
 @brief A localization key that is used to update the text of a UITextField object.
 */
@property (nonatomic, copy) IBInspectable NSString *localizationKey;

#pragma mark - Placeholder localization
/**
 @brief A localization key that is used to update the placeholder of a UITextField object.
 */
@property (nonatomic, copy) IBInspectable NSString *placeholderLocalizationKey;

@end
