//
//  UISearchBar+LNTXLocalization.m
//  Alpha
//
//  Created by Damien Rambout on 11/03/15.
//  Copyright (c) 2015 LINITIX. All rights reserved.
//

#import "UISearchBar+LNTXLocalization.h"

static NSString * const kTextKeyPath = @"text";
static NSString * const kPlaceholderKeyPath = @"placeholder";

@implementation UISearchBar (LNTXLocalization)

#pragma mark - Public accessors

- (NSString *)localizationKey {
    return [self localizationKeyForKeyPath:kTextKeyPath];
}

- (void)setLocalizationKey:(NSString *)localizationKey {
    [self setLocalizationKey:localizationKey forKeyPath:kTextKeyPath];
}

- (NSString *)placeholderLocalizationKey {
    return [self localizationKeyForKeyPath:kPlaceholderKeyPath];
}

- (void)setPlaceholderLocalizationKey:(NSString *)placeholderLocalizationKey {
    [self setLocalizationKey:placeholderLocalizationKey forKeyPath:kPlaceholderKeyPath];
}

@end
