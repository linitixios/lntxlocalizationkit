//
//  UIBarItem+LNTXLocalization.m
//  LNTXLocalizationKit
//
//  Created by Damien Rambout on 17/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import "UIBarItem+LNTXLocalization.h"

static NSString * const kTitleKeyPath = @"title";

@implementation UIBarItem (LNTXLocalization)

#pragma mark - Public accessors

- (NSString *)localizationKey {
    return [self localizationKeyForKeyPath:kTitleKeyPath];
}

- (void)setLocalizationKey:(NSString *)localizationKey {
    [self setLocalizationKey:localizationKey forKeyPath:kTitleKeyPath];
}

@end
