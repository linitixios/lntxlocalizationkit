//
//  UIButton+LNTXLocalization.h
//  LNTXLocalizationKit
//
//  Created by Damien Rambout on 05/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <LNTXLocalizationKit/NSObject+LNTXLocalization.h>

/**
 @brief Extension with properties and methods for localizing a UIButton object.
 */
@interface UIButton (LNTXLocalization)

#pragma mark - Title localization
/** 
 @brief A localization key that is used to update the normal title of a UIButton object.
 This title can then be retrieve using [button titleForState:UIControlStateNormal].
 */
@property (nonatomic, copy) IBInspectable NSString *localizationKey;
/** 
 @brief A localization key that is used to update the highlighted title of a UIButton object.
 This title can then be retrieve using [button titleForState:UIControlStateHighlighted].
 */
@property (nonatomic, copy) IBInspectable NSString *highlightedLocalizationKey;
/** 
 @brief A localization key that is used to update the selected title of a UIButton object.
 This title can then be retrieve using [button titleForState:UIControlStateSelected].
 */
@property (nonatomic, copy) IBInspectable NSString *selectedLocalizationKey;
/** 
 @brief A localization key that is used to update the disabled title of a UIButton object.
 This title can then be retrieve using [button titleForState:UIControlStateDisabled].
 */
@property (nonatomic, copy) IBInspectable NSString *disabledLocalizationKey;

@end
