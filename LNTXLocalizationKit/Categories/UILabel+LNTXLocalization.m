//
//  UILabel+LNTXLocalization.m
//  LNTXLocalizationKit
//
//  Created by Damien Rambout on 05/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import "UILabel+LNTXLocalization.h"

#import "NSObject+LNTXLocalization.h"

static NSString * const kLocalizationKeyKeyPath = @"text";

@implementation UILabel (LNTXLocalization)

- (void)setLocalizationKey:(NSString *)localizationKey {
    [self setLocalizationKey:localizationKey forKeyPath:kLocalizationKeyKeyPath];
}

- (NSString *)localizationKey {
    return [self localizationKeyForKeyPath:kLocalizationKeyKeyPath];
}

@end
