//
//  UITextView+LNTXLocalization.m
//  LNTXLocalizationKit
//
//  Created by Damien Rambout on 17/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import "UITextView+LNTXLocalization.h"

static NSString * const kTextKeyPath = @"text";

@implementation UITextView (LNTXLocalization)

#pragma mark - Public accessors

- (NSString *)localizationKey {
    return [self localizationKeyForKeyPath:kTextKeyPath];
}

- (void)setLocalizationKey:(NSString *)localizationKey {
    [self setLocalizationKey:localizationKey forKeyPath:kTextKeyPath];
}

@end
