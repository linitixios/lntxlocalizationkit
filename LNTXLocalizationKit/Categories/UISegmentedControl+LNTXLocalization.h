//
//  UISegmentedControl+LNTXLocalization.h
//  LNTXLocalizationKit
//
//  Created by Damien Rambout on 17/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <LNTXLocalizationKit/NSObject+LNTXLocalization.h>

/**
 @brief Extension with properties and methods for localizing a UISegmentedControl object.
 */
@interface UISegmentedControl (LNTXLocalization)

#pragma mark - Titles localization
/**
 @brief A localization key prefix that is used to update the titles of a UISegmentedControl object.
 The value of this property is simply a prefix used to retrieve localized values by appending
 the title's index to make the full localization key. The index starts at 0.
 For instance, if your prefix is "segmented_control_", and your segmented control has 2 indexes,
 you must have defined 2 localization key "segmented_control_0" and "segmented_control_1".
 */
@property (nonatomic, copy) IBInspectable NSString *localizationKeyPrefix;

/**
 @brief Returns the localization key that can be used to retrieve the localized value for a specific segment. 
 If the localizationKeyPrefix property has not been previously set, then it returns nil.
 @return The localization key that can be used to retrieve the localized value for a specific segment.
 */
- (NSString *)titleLocalizationKeyForSegmentAtIndex:(NSUInteger)index;

@end
