//
//  UISearchBar+LNTXLocalization.h
//  Alpha
//
//  Created by Damien Rambout on 11/03/15.
//  Copyright (c) 2015 LINITIX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <LNTXLocalizationKit/NSObject+LNTXLocalization.h>

@interface UISearchBar (LNTXLocalization)

/**
 @brief Extension with properties and methods for localizing a UISearchBar object.
 */
#pragma mark - Title localization
/**
 @brief A localization key that is used to update the text of a UISearchBar object.
 */
@property (nonatomic, copy) IBInspectable NSString *localizationKey;

#pragma mark - Placeholder localization
/**
 @brief A localization key that is used to update the placeholder of a UISearchBar object.
 */
@property (nonatomic, copy) IBInspectable NSString *placeholderLocalizationKey;

@end
