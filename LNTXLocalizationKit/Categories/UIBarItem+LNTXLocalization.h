//
//  UIBarItem+LNTXLocalization.h
//  LNTXLocalizationKit
//
//  Created by Damien Rambout on 17/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <LNTXLocalizationKit/NSObject+LNTXLocalization.h>

/**
 @brief Extension with properties and methods for localizing a UIBarItem object.
 */
@interface UIBarItem (LNTXLocalization)

#pragma mark - Title localization
/**
 @brief A localization key that is used to update the title of a UIBarItem object.
 */
@property (nonatomic, copy) IBInspectable NSString *localizationKey;

@end
