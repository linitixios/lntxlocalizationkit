//
//  UIButton+LNTXLocalization.m
//  LNTXLocalizationKit
//
//  Created by Damien Rambout on 05/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import "UIButton+LNTXLocalization.h"

#import <objc/runtime.h>

static NSString * const kNormalTitleKeyPath = @"normalTitle";
static NSString * const kHighlightedTitleKeyPath = @"highlightedTitle";
static NSString * const kSelectedTitleKeyPath = @"selectedTitle";
static NSString * const kDisabledTitleKeyPath = @"disabledTitle";

@interface UIButton ()

@property (nonatomic) NSString *normalTitle;
@property (nonatomic) NSString *highlightedTitle;
@property (nonatomic) NSString *selectedTitle;
@property (nonatomic) NSString *disabledTitle;

@end

@implementation UIButton (LNTXLocalization)

#pragma mark - Public accessors

- (NSString *)localizationKey {
    return [self localizationKeyForKeyPath:kNormalTitleKeyPath];
}

- (void)setLocalizationKey:(NSString *)localizationKey {
    [self setLocalizationKey:localizationKey forKeyPath:kNormalTitleKeyPath];
}

- (NSString *)highlightedLocalizationKey {
    return [self localizationKeyForKeyPath:kHighlightedTitleKeyPath];
}

- (void)setHighlightedLocalizationKey:(NSString *)highlightedLocalizationKey {
    [self setLocalizationKey:highlightedLocalizationKey forKeyPath:kHighlightedTitleKeyPath];
}

- (NSString *)selectedLocalizationKey {
    return [self localizationKeyForKeyPath:kSelectedTitleKeyPath];
}

- (void)setSelectedLocalizationKey:(NSString *)selectedLocalizationKey {
    [self setLocalizationKey:selectedLocalizationKey forKeyPath:kSelectedTitleKeyPath];
}

- (NSString *)disabledLocalizationKey {
    return [self localizationKeyForKeyPath:kDisabledTitleKeyPath];
}

- (void)setDisabledLocalizationKey:(NSString *)disabledLocalizationKey {
    [self setLocalizationKey:disabledLocalizationKey forKeyPath:kDisabledTitleKeyPath];
}

#pragma mark - Private accessors

- (NSString *)normalTitle {
    return [self titleForState:UIControlStateNormal];
}

- (void)setNormalTitle:(NSString *)normalTitle {
    [self setTitle:normalTitle forState:UIControlStateNormal];
}

- (NSString *)highlightedTitle {
    return [self titleForState:UIControlStateHighlighted];
}

- (void)setHighlightedTitle:(NSString *)highlightedTitle {
    [self setTitle:highlightedTitle forState:UIControlStateHighlighted];
}

- (NSString *)selectedTitle {
    return [self titleForState:UIControlStateSelected];
}

- (void)setSelectedTitle:(NSString *)selectedTitle {
    [self setTitle:selectedTitle forState:UIControlStateSelected];
}

- (NSString *)disabledTitle {
    return [self titleForState:UIControlStateDisabled];
}

- (void)setDisabledTitle:(NSString *)disabledTitle {
    [self setTitle:disabledTitle forState:UIControlStateDisabled];
}

@end
