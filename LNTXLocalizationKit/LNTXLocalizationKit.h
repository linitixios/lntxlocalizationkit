//
//  LNTXLocalizationKit.h
//  LNTXLocalizationKit
//
//  Created by Damien Rambout on 12/03/15.
//  Copyright (c) 2015 Linitix. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for LNTXLocalizationKit.
FOUNDATION_EXPORT double LNTXLocalizationKitVersionNumber;

//! Project version string for LNTXLocalizationKit.
FOUNDATION_EXPORT const unsigned char LNTXLocalizationKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <LNTXLocalizationKit/PublicHeader.h>

#import <LNTXLocalizationKit/LNTXLocalizationManager.h>
#import <LNTXLocalizationKit/NSObject+LNTXLocalization.h>
#import <LNTXLocalizationKit/UILabel+LNTXLocalization.h>
#import <LNTXLocalizationKit/UIButton+LNTXLocalization.h>
#import <LNTXLocalizationKit/UIViewController+LNTXLocalization.h>
#import <LNTXLocalizationKit/UINavigationItem+LNTXLocalization.h>
#import <LNTXLocalizationKit/UIBarItem+LNTXLocalization.h>
#import <LNTXLocalizationKit/UISegmentedControl+LNTXLocalization.h>
#import <LNTXLocalizationKit/UITextView+LNTXLocalization.h>
#import <LNTXLocalizationKit/UITextField+LNTXLocalization.h>
#import <LNTXLocalizationKit/UISearchBar+LNTXLocalization.h>
