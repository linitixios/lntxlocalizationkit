//
//  NSObject+LNTXLocalization.m
//  LNTXLocalizationKit
//
//  Created by Damien Rambout on 04/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import "NSObject+LNTXLocalization.h"

#import <objc/runtime.h>

#import "LNTXLocalizationManager+Private.h"

static void * const kLocalizationTableKey = (__bridge void * const)@"lntx_localizationTable";
static void * const kLocalizationKeysKey = (__bridge void * const)@"lntx_localizationKey";
static void * const kDeallocCallerKey = (__bridge void * const)@"lntx_deallocCaller";

@interface LNTXLocalizationDeallocCaller : NSObject

@property (nonatomic, copy) void(^deallocBlock)();

@end

@implementation LNTXLocalizationDeallocCaller

- (id)initWithBlock:(void(^)())block {
    if (self = [super init]) {
        self.deallocBlock = block;
    }
    
    return self;
}

- (void)dealloc {
    if (self.deallocBlock) {
        self.deallocBlock();
    }
}

@end

@interface NSObject ()

@property (nonatomic, readonly) NSMutableDictionary *localizationKeys;
@property (nonatomic) LNTXLocalizationDeallocCaller *deallocCaller;

@end

@implementation NSObject (LNTXLocalization)

- (void)localize {
    // Localize all keys
    [self localizeForKeyPaths:[self.localizationKeys allKeys]];
}

- (void)localizeForKeyPaths:(NSArray *)keyPaths {
    // Localize specific keys
    NSDictionary *localizationKeys = self.localizationKeys;
    for (NSString *keyPath in keyPaths) {
        NSString *localizationKey = localizationKeys[keyPath];
        NSAssert(localizationKey, @"Localization key associated with specified keypath (%@) must not be nil.", keyPath);
        
        // Update localized value at keypath
        NSString *localizedValue = LNTXLocalizedStringFromTable(localizationKey, self.localizationTable);
        [self setValue:localizedValue forKey:keyPath];
    }
}

- (void)registerForLocalizationUpdatesIfNeeded {
    if (!self.deallocCaller) {
        __weak typeof(self) weakSelf = self;
        // Register for notifications
        id localizationUpdatesObserver = [[NSNotificationCenter defaultCenter] addObserverForName:LNTXInternalLanguageDidUpdateNotification
                                                                                           object:nil
                                                                                            queue:[NSOperationQueue mainQueue]
                                                                                       usingBlock:^(NSNotification *note) {
                                                                                           [weakSelf localize];
                                                                                       }];
        
        self.deallocCaller = [[LNTXLocalizationDeallocCaller alloc] initWithBlock:^{
            // Unregister from notifications
            [[NSNotificationCenter defaultCenter] removeObserver:localizationUpdatesObserver];
        }];
    }
}

#pragma mark - Accessors

- (NSMutableDictionary *)localizationKeys {
    NSMutableDictionary *localizationKeys = objc_getAssociatedObject(self, kLocalizationKeysKey);
    
    if (!localizationKeys) {
        localizationKeys = [[NSMutableDictionary alloc] init];
        objc_setAssociatedObject(self, kLocalizationKeysKey, localizationKeys, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    
    return localizationKeys;
}

- (void)setLocalizationTable:(NSString *)localizationTable {
    objc_setAssociatedObject(self, kLocalizationTableKey,
                             localizationTable, OBJC_ASSOCIATION_COPY);
    [self localize];
}

- (NSString *)localizationTable {
   return objc_getAssociatedObject(self, kLocalizationTableKey);
}

- (void)setDeallocCaller:(LNTXLocalizationDeallocCaller *)deallocCaller {
    objc_setAssociatedObject(self, kDeallocCallerKey, deallocCaller, OBJC_ASSOCIATION_RETAIN);
}

- (LNTXLocalizationDeallocCaller *)deallocCaller {
    return objc_getAssociatedObject(self, kDeallocCallerKey);
}

#pragma mark - Methods

- (void)setLocalizationKey:(NSString *)localizationKey forKeyPath:(NSString *)keyPath {
    NSParameterAssert(keyPath);
    
    if (localizationKey) {
        self.localizationKeys[keyPath] = localizationKey;
        [self registerForLocalizationUpdatesIfNeeded];
        [self localizeForKeyPaths:@[keyPath]];
    } else {
        [self.localizationKeys removeObjectForKey:keyPath];
    }
}

- (NSString *)localizationKeyForKeyPath:(NSString *)keyPath {
    return self.localizationKeys[keyPath];
}

@end
