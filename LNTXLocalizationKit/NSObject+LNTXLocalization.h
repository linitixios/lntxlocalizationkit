//
//  NSObject+LNTXLocalization.h
//  LNTXLocalizationKit
//
//  Created by Damien Rambout on 04/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @brief Extension with properties and methods for localizing a NSObject object.
 To localize properties of a NSObject subclass, use the method `-setLocalizationKey:forKeyPath:` by providing the keypath
 of the property to localize. If localization keys are located in another localization table, set the 
 `localizationTable` to the localization table's name.
 Localized properties will be automatically updated on `LNTXLanguageDidUpdateNotification` notifications.
 */
@interface NSObject (LNTXLocalization)

/** 
 @brief The localization table in which the localized values must be read. 
 Its default value is nil, which means the default localization table.
 */
@property (nonatomic, copy) NSString *localizationTable;

/** 
 @brief Sets a localization key for a specific key path.
 @param localizationKey A localization key defined in a localization table. If nil is provided, then the localization is removed for the keypath provided.
 @param keyPath The keypath for which the localization key is set. This parameter must not be nil.
 */
- (void)setLocalizationKey:(NSString *)localizationKey forKeyPath:(NSString *)keyPath;

/** 
 @brief Returns the localization key associated with a specific keypath. Is no localization key has been associated for the keypath, then nil is returned.
 @return The localization key associated with a specific keypath.
 */
- (NSString *)localizationKeyForKeyPath:(NSString *)keyPath;

@end
