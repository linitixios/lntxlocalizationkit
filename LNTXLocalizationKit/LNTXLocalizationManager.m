//
//  LNTXLocalizationManager.m
//  LNTXLocalizationKit
//
//  Created by Damien Rambout on 04/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import "LNTXLocalizationManager.h"

#import "LNTXLocalizationManager+Private.h"

static NSString * const kUnsupportedLanguageException = @"LNTXUnsupportedLanguageException";
static NSString * const kLanguageNotAllowedException = @"LNTXLanguageNotAllowedException";

NSString * const LNTXLanguageWillUpdateNotification = @"LNTXLanguageWillUpdateNotification";
NSString * const LNTXLanguageDidUpdateNotification = @"LNTXLanguageDidUpdateNotification";
NSString * const LNTXPreviousLanguageUserInfoKey = @"LNTXPreviousLanguageUserInfoKey";
NSString * const LNTXCurrentLanguageUserInfoKey = @"LNTXCurrentLanguageUserInfoKey";

@implementation LNTXLocalizationManager

@synthesize currentLanguage = _currentLanguage;

#pragma mark - Singleton

+ (instancetype)sharedManager {
    static id sharedManager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    
    return sharedManager;
}

#pragma mark - Static methods

+ (NSArray *)supportedLanguages {
    NSMutableArray *localizations = [NSBundle mainBundle].localizations.mutableCopy;
    NSMutableArray *sortedLocalizations = [NSMutableArray arrayWithCapacity:localizations.count];
    
    while (localizations.count > 0) {
        NSArray *preferredLocalizations = [NSBundle preferredLocalizationsFromArray:localizations];
        [localizations removeObjectsInArray:preferredLocalizations];
        [sortedLocalizations addObjectsFromArray:preferredLocalizations];
    }
    
    return [sortedLocalizations copy];
}

+ (NSString *)preferredLanguage {
    NSArray *supportedLanguages = [self supportedLanguages];
    return [NSBundle preferredLocalizationsFromArray:supportedLanguages].firstObject;
}

#pragma mark - Accessors

- (NSString *)currentLanguage {
    return _currentLanguage ?: [self.class preferredLanguage];
}

- (void)setCurrentLanguage:(NSString *)currentLanguage {
    if (currentLanguage) {
        if (![[self.class supportedLanguages] containsObject:currentLanguage]) {
            [NSException raise:kUnsupportedLanguageException
                        format:@"Trying to set unsupported language: %@", currentLanguage];
        }
        
        if (self.allowedLanguages && ![self.allowedLanguages containsObject:currentLanguage]) {
            [NSException raise:kLanguageNotAllowedException
                        format:@"Trying to set a language (%@) that is not allowed (%@).",
             currentLanguage, self.allowedLanguages];
        }
    }
    
    if (_currentLanguage != currentLanguage) {
        NSString *newLanguage = currentLanguage ?: [self.class preferredLanguage];
        NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithDictionary:@{LNTXCurrentLanguageUserInfoKey: newLanguage}];
        if (_currentLanguage) {
            userInfo[LNTXPreviousLanguageUserInfoKey] = _currentLanguage;
        }
        
        [self postWillChangeLanguageNotificationsWithUserInfo:[userInfo copy]];
        
        _currentLanguage = newLanguage;
        
        [self postDidChangeLanguageNotificationsWithUserInfo:userInfo];
    }
}

- (void)setAllowedLanguages:(NSSet *)allowedLanguages {
    _allowedLanguages = [allowedLanguages copy];
    
    if (allowedLanguages) {
        // Non nil
        NSAssert(allowedLanguages.count > 0,
                 @"Allowed languages must contain at least one language.");
        NSArray *supportedLanguages = [self.class supportedLanguages];
        NSAssert([allowedLanguages isSubsetOfSet:[NSSet setWithArray:supportedLanguages]],
                 @"Allowed languages must be a subset of project's supported languages.");
        
        for (NSString *supportedLanguage in supportedLanguages) {
            if ([allowedLanguages containsObject:supportedLanguage]) {
                self.currentLanguage = supportedLanguage;
                break;
            }
        }
    } else {
        // Nil
        self.currentLanguage = nil;
    }
}

- (NSBundle *)currentBundle {
    NSString *bundlePath = [[NSBundle mainBundle] pathForResource:self.currentLanguage ofType:@"lproj"];
    return [NSBundle bundleWithPath:bundlePath];
}

- (NSString *)localizedStringForKey:(NSString *)key {
    return [[self currentBundle] localizedStringForKey:key value:@"" table:nil];
}

- (NSString *)localizedStringForKey:(NSString *)key fromTable:(NSString *)table {
    return [[self currentBundle] localizedStringForKey:key value:@"" table:table];
}

- (NSString *)localizedPathForResource:(NSString *)name ofType:(NSString *)extension {
    NSString *path = [self.currentBundle pathForResource:name ofType:extension];
    return path ?: [[NSBundle mainBundle] pathForResource:name ofType:extension];
}


#pragma mark - Notifications

- (void)postWillChangeLanguageNotificationsWithUserInfo:(NSDictionary *)userInfo
{
    // Will change notification
    [[NSNotificationCenter defaultCenter] postNotificationName:LNTXLanguageWillUpdateNotification
                                                        object:self
                                                      userInfo:userInfo];
}

- (void)postDidChangeLanguageNotificationsWithUserInfo:(NSDictionary *)userInfo
{
    // Internal did change notification
    [[NSNotificationCenter defaultCenter] postNotificationName:LNTXInternalLanguageDidUpdateNotification
                                                        object:self
                                                      userInfo:userInfo];
    
    // Did change notification
    [[NSNotificationCenter defaultCenter] postNotificationName:LNTXLanguageDidUpdateNotification
                                                        object:self
                                                      userInfo:userInfo];
}

@end
