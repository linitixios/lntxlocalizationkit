//
//  UIButtonLocalizationTests.m
//  LNTXLocalizationKit
//
//  Created by Damien Rambout on 05/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "UIButton+LNTXLocalization.h"
#import "LNTXLocalizationManagerMock.h"
#import "NSBundle+Tests.h"

static NSString * const kLocalizationKey = @"some_localization_key";
static NSString * const kLocalizationTable = @"SomeLocalizationTable";

@interface UIButtonLocalizationTests : XCTestCase

@property (nonatomic) UIButton *button;

@end

@implementation UIButtonLocalizationTests

+ (void)setUp {
    // Swizzle to get a correct main bundle in test environment
    [NSBundle lntx_switchBundleMethods];
    [LNTXLocalizationManagerMock switchSharedManagerMethods];
}

+ (void)tearDown {
    [NSBundle lntx_switchBundleMethods];
    [LNTXLocalizationManagerMock switchSharedManagerMethods];
}

- (void)setUp
{
    [super setUp];

    self.button = [[UIButton alloc] init];
}

- (void)tearDown
{
    self.button = nil;
    [LNTXLocalizationManagerMock resetSharedInstance];

    [super tearDown];
}

#pragma mark - Localization key

- (void)testLocalizationKeyIsSettable {
    self.button.localizationKey = kLocalizationKey;
    
    XCTAssertEqualObjects(self.button.localizationKey, kLocalizationKey,
                          @"Localization key should be settable and return the same value set previously.");
}

- (void)testLocalizedTitleIsEqualToLocalizedValue {
    self.button.localizationKey = kLocalizationKey;
    NSString *localizedValue = LNTXLocalizedString(kLocalizationKey);
    NSString *normalTitle = [self.button titleForState:UIControlStateNormal];
    
    XCTAssertEqualObjects(normalTitle, localizedValue,
                          @"Localized text (%@) should be equal to the localized value (%@).",
                          normalTitle, localizedValue);
}

- (void)testLocalizedTitleIsEqualToLocalizedValueFromTable {
    self.button.localizationTable = kLocalizationTable;
    self.button.localizationKey = kLocalizationKey;
    NSString *localizedValue = LNTXLocalizedStringFromTable(kLocalizationKey, kLocalizationTable);
    NSString *normalTitle = [self.button titleForState:UIControlStateNormal];
    
    XCTAssertEqualObjects(normalTitle, localizedValue,
                          @"Localized text (%@) should be equal to the localized value (%@).",
                          normalTitle, localizedValue);
}

#pragma mark - Highlighted localization key

- (void)testHighlightedLocalizationKeyIsSettable {
    self.button.highlightedLocalizationKey = kLocalizationKey;
    
    XCTAssertEqualObjects(self.button.highlightedLocalizationKey, kLocalizationKey,
                          @"Localization key should be settable and return the same value set previously.");
}

- (void)testHighlightedLocalizedTitleIsEqualToLocalizedValue {
    self.button.highlightedLocalizationKey = kLocalizationKey;
    NSString *localizedValue = LNTXLocalizedString(kLocalizationKey);
    NSString *highlightedTitle = [self.button titleForState:UIControlStateHighlighted];
    
    XCTAssertEqualObjects(highlightedTitle, localizedValue,
                          @"Localized text (%@) should be equal to the localized value (%@).",
                          highlightedTitle, localizedValue);
}

- (void)testHighlightedLocalizedTitleIsEqualToLocalizedValueFromTable {
    self.button.localizationTable = kLocalizationTable;
    self.button.highlightedLocalizationKey = kLocalizationKey;
    NSString *localizedValue = LNTXLocalizedStringFromTable(kLocalizationKey, kLocalizationTable);
    NSString *highlightedTitle = [self.button titleForState:UIControlStateHighlighted];
    
    XCTAssertEqualObjects(highlightedTitle, localizedValue,
                          @"Localized text (%@) should be equal to the localized value (%@).",
                          highlightedTitle, localizedValue);
}

#pragma mark - Selected localization key

- (void)testSelectedLocalizationKeyIsSettable {
    self.button.selectedLocalizationKey = kLocalizationKey;
    
    XCTAssertEqualObjects(self.button.selectedLocalizationKey, kLocalizationKey,
                          @"Localization key should be settable and return the same value set previously.");
}

- (void)testSelectedLocalizedTitleIsEqualToLocalizedValue {
    self.button.selectedLocalizationKey = kLocalizationKey;
    NSString *localizedValue = LNTXLocalizedString(kLocalizationKey);
    NSString *selectedTitle = [self.button titleForState:UIControlStateSelected];
    
    XCTAssertEqualObjects(selectedTitle, localizedValue,
                          @"Localized text (%@) should be equal to the localized value (%@).",
                          selectedTitle, localizedValue);
}

- (void)testSelectedLocalizedTitleIsEqualToLocalizedValueFromTable {
    self.button.localizationTable = kLocalizationTable;
    self.button.selectedLocalizationKey = kLocalizationKey;
    NSString *localizedValue = LNTXLocalizedStringFromTable(kLocalizationKey, kLocalizationTable);
    NSString *selectedTitle = [self.button titleForState:UIControlStateSelected];
    
    XCTAssertEqualObjects(selectedTitle, localizedValue,
                          @"Localized text (%@) should be equal to the localized value (%@).",
                          selectedTitle, localizedValue);
}

#pragma mark - Disabled localization key

- (void)testDisabledLocalizationKeyIsSettable {
    self.button.disabledLocalizationKey = kLocalizationKey;
    
    XCTAssertEqualObjects(self.button.disabledLocalizationKey, kLocalizationKey,
                          @"Localization key should be settable and return the same value set previously.");
}

- (void)testDisabledLocalizedTitleIsEqualToLocalizedValue {
    self.button.disabledLocalizationKey = kLocalizationKey;
    NSString *localizedValue = LNTXLocalizedString(kLocalizationKey);
    NSString *disabledTitle = [self.button titleForState:UIControlStateDisabled];
    
    XCTAssertEqualObjects(disabledTitle, localizedValue,
                          @"Localized text (%@) should be equal to the localized value (%@).",
                          disabledTitle, localizedValue);
}

- (void)testDisabledLocalizedTitleIsEqualToLocalizedValueFromTable {
    self.button.localizationTable = kLocalizationTable;
    self.button.disabledLocalizationKey = kLocalizationKey;
    NSString *localizedValue = LNTXLocalizedStringFromTable(kLocalizationKey, kLocalizationTable);
    NSString *disabledTitle = [self.button titleForState:UIControlStateDisabled];
    
    XCTAssertEqualObjects(disabledTitle, localizedValue,
                          @"Localized text (%@) should be equal to the localized value (%@).",
                          disabledTitle, localizedValue);
}

@end
