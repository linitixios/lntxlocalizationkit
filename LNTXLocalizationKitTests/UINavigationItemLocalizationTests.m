//
//  UINavigationItemLocalizationTests.m
//  LNTXLocalizationKit
//
//  Created by Damien Rambout on 17/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "LNTXLocalizationKitTestCase.h"

#import "UINavigationItem+LNTXLocalization.h"

@interface UINavigationItemLocalizationTests : LNTXLocalizationKitTestCase

@property (nonatomic) UINavigationItem *navigationItem;

@end

@implementation UINavigationItemLocalizationTests

- (void)setUp
{
    [super setUp];
    
    self.navigationItem = [[UINavigationItem alloc] init];
}

- (void)tearDown
{
    self.navigationItem = nil;
    
    [super tearDown];
}

#pragma mark - Localization key

- (void)testLocalizationKeyIsSettable {
    self.navigationItem.localizationKey = LNTXLocalizationKey;
    
    XCTAssertEqualObjects(self.navigationItem.localizationKey, LNTXLocalizationKey,
                          @"Localization key should be settable and return the same value set previously.");
}

- (void)testLocalizedTitleIsEqualToLocalizedValue {
    self.navigationItem.localizationKey = LNTXLocalizationKey;
    NSString *localizedValue = LNTXLocalizedString(LNTXLocalizationKey);
    NSString *text = [self.navigationItem title];
    
    XCTAssertEqualObjects(text, localizedValue,
                          @"Localized text (%@) should be equal to the localized value (%@).",
                          text, localizedValue);
}

- (void)testLocalizedTitleIsEqualToLocalizedValueFromTable {
    self.navigationItem.localizationTable = LNTXLocalizationTable;
    self.navigationItem.localizationKey = LNTXLocalizationKey;
    NSString *localizedValue = LNTXLocalizedStringFromTable(LNTXLocalizationKey, LNTXLocalizationTable);
    NSString *text = [self.navigationItem title];
    
    XCTAssertEqualObjects(text, localizedValue,
                          @"Localized text (%@) should be equal to the localized value (%@).",
                          text, localizedValue);
}

@end
