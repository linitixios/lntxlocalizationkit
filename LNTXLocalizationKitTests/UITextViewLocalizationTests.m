//
//  UITextViewLocalizationTests.m
//  LNTXLocalizationKit
//
//  Created by Damien Rambout on 17/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "UITextView+LNTXLocalization.h"
#import "LNTXLocalizationManagerMock.h"
#import "NSBundle+Tests.h"

static NSString * const kLocalizationKey = @"some_localization_key";
static NSString * const kLocalizationTable = @"SomeLocalizationTable";

@interface UITextViewLocalizationTests : XCTestCase

@property (nonatomic) UITextView *textView;

@end

@implementation UITextViewLocalizationTests

+ (void)setUp {
    // Swizzle to get a correct main bundle in test environment
    [NSBundle lntx_switchBundleMethods];
    [LNTXLocalizationManagerMock switchSharedManagerMethods];
}

+ (void)tearDown {
    [NSBundle lntx_switchBundleMethods];
    [LNTXLocalizationManagerMock switchSharedManagerMethods];
}

- (void)setUp
{
    [super setUp];

    self.textView = [[UITextView alloc] init];
}

- (void)tearDown
{
    self.textView = nil;
    
    [super tearDown];
}

#pragma mark - Localization key

- (void)testLocalizationKeyIsSettable {
    self.textView.localizationKey = kLocalizationKey;
    
    XCTAssertEqualObjects(self.textView.localizationKey, kLocalizationKey,
                          @"Localization key should be settable and return the same value set previously.");
}

- (void)testLocalizedTitleIsEqualToLocalizedValue {
    self.textView.localizationKey = kLocalizationKey;
    NSString *localizedValue = LNTXLocalizedString(kLocalizationKey);
    NSString *text = [self.textView text];
    
    XCTAssertEqualObjects(text, localizedValue,
                          @"Localized text (%@) should be equal to the localized value (%@).",
                          text, localizedValue);
}

- (void)testLocalizedTitleIsEqualToLocalizedValueFromTable {
    self.textView.localizationTable = kLocalizationTable;
    self.textView.localizationKey = kLocalizationKey;
    NSString *localizedValue = LNTXLocalizedStringFromTable(kLocalizationKey, kLocalizationTable);
    NSString *text = [self.textView text];
    
    XCTAssertEqualObjects(text, localizedValue,
                          @"Localized text (%@) should be equal to the localized value (%@).",
                          text, localizedValue);
}

@end
