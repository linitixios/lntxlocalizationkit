//
//  UITextFieldLocalizationTests.m
//  LNTXLocalizationKit
//
//  Created by Damien Rambout on 17/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "UITextField+LNTXLocalization.h"
#import "LNTXLocalizationManagerMock.h"
#import "NSBundle+Tests.h"

static NSString * const kLocalizationKey = @"some_localization_key";
static NSString * const kLocalizationTable = @"SomeLocalizationTable";

@interface UITextFieldLocalizationTests : XCTestCase

@property (nonatomic) UITextField *textField;

@end

@implementation UITextFieldLocalizationTests

+ (void)setUp {
    // Swizzle to get a correct main bundle in test environment
    [NSBundle lntx_switchBundleMethods];
    [LNTXLocalizationManagerMock switchSharedManagerMethods];
}

+ (void)tearDown {
    [NSBundle lntx_switchBundleMethods];
    [LNTXLocalizationManagerMock switchSharedManagerMethods];
}

- (void)setUp
{
    [super setUp];
    
    self.textField = [[UITextField alloc] init];
}

- (void)tearDown
{
    self.textField = nil;
    
    [super tearDown];
}

#pragma mark - Localization key

- (void)testLocalizationKeyIsSettable {
    self.textField.localizationKey = kLocalizationKey;
    
    XCTAssertEqualObjects(self.textField.localizationKey, kLocalizationKey,
                          @"Localization key should be settable and return the same value set previously.");
}

- (void)testLocalizedTitleIsEqualToLocalizedValue {
    self.textField.localizationKey = kLocalizationKey;
    NSString *localizedValue = LNTXLocalizedString(kLocalizationKey);
    NSString *normalTitle = [self.textField text];
    
    XCTAssertEqualObjects(normalTitle, localizedValue,
                          @"Localized text (%@) should be equal to the localized value (%@).",
                          normalTitle, localizedValue);
}

- (void)testLocalizedTitleIsEqualToLocalizedValueFromTable {
    self.textField.localizationTable = kLocalizationTable;
    self.textField.localizationKey = kLocalizationKey;
    NSString *localizedValue = LNTXLocalizedStringFromTable(kLocalizationKey, kLocalizationTable);
    NSString *normalTitle = [self.textField text];
    
    XCTAssertEqualObjects(normalTitle, localizedValue,
                          @"Localized text (%@) should be equal to the localized value (%@).",
                          normalTitle, localizedValue);
}

#pragma mark - Placeholder localization key

- (void)testPlaceholderLocalizationKeyIsSettable {
    self.textField.placeholderLocalizationKey = kLocalizationKey;
    
    XCTAssertEqualObjects(self.textField.placeholderLocalizationKey, kLocalizationKey,
                          @"Placeholder localization key should be settable and return the same value set previously.");
}

- (void)testPlaceholderLocalizedTitleIsEqualToLocalizedValue {
    self.textField.placeholderLocalizationKey = kLocalizationKey;
    NSString *localizedValue = LNTXLocalizedString(kLocalizationKey);
    NSString *placeholder = [self.textField placeholder];
    
    XCTAssertEqualObjects(placeholder, localizedValue,
                          @"Localized placeholder (%@) should be equal to the localized value (%@).",
                          placeholder, localizedValue);
}

- (void)testPlaceholderLocalizedTitleIsEqualToLocalizedValueFromTable {
    self.textField.localizationTable = kLocalizationTable;
    self.textField.placeholderLocalizationKey = kLocalizationKey;
    NSString *localizedValue = LNTXLocalizedStringFromTable(kLocalizationKey, kLocalizationTable);
    NSString *placeholder = [self.textField placeholder];
    
    XCTAssertEqualObjects(placeholder, localizedValue,
                          @"Localized placeholder (%@) should be equal to the localized value (%@).",
                          placeholder, localizedValue);
}

@end
