//
//  NSObjectLocalizationTests.m
//  LNTXLocalizationKit
//
//  Created by Damien Rambout on 04/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "NSObject+LNTXLocalization.h"
#import "NSObject+Tests.h"
#import "LNTXLocalizationManagerMock.h"
#import "NSBundle+Tests.h"

@interface NSObjectLocalizationTests : XCTestCase

@property (nonatomic) NSObject *object;

@end

static NSString * const kRandomLocalizationTable = @"Some table";
static NSString * const kRandomLocalizationKey = @"Localiation key";
static NSString * const kRandomKeyPath = @"Key path";

static NSString * const kLocalizationKey = @"some_localization_key";
static NSString * const kLocalizationTable = @"SomeLocalizationTable";

@implementation NSObjectLocalizationTests

+ (void)setUp {
    // Swizzle to get a correct main bundle in test environment
    [NSBundle lntx_switchBundleMethods];
    // Swizzle to get a correct language manager
    [LNTXLocalizationManagerMock switchSharedManagerMethods];
}

+ (void)tearDown {
    [NSBundle lntx_switchBundleMethods];
    [LNTXLocalizationManagerMock switchSharedManagerMethods];
}

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
    self.object = [[NSObject alloc] init];
}

- (void)tearDown
{
    self.object = nil;
    [LNTXLocalizationManagerMock resetSharedInstance];
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

#pragma mark - Localization table

- (void)testLocalizationTableIsNilByDefault {
    XCTAssertNil(self.object.localizationTable, @"Localization table should be nil by default");
}

- (void)testLocalizationTableIsSettable {
    self.object.localizationTable = kRandomLocalizationTable;
    
    XCTAssertEqual(self.object.localizationTable, kRandomLocalizationTable,
                   @"Localization table should be settable and return the same value set previously.");
}

#pragma mark - Localization key

- (void)testObjectThrowsExceptionWhenKeyPathIsInvalid {
    XCTAssertThrows([self.object setLocalizationKey:kRandomLocalizationKey forKeyPath:kRandomKeyPath],
                    @"Object should throw an exception when the keypath is invalid.");
}

- (void)testObjectIsNotLocalizedByDefault {
    XCTAssertNil([self.object localizationKeyForKeyPath:kRandomKeyPath], @"Localized object should not be localized by default");
}

- (void)testLocalizationKeyIsSettable {
    [self.object setLocalizationKey:kRandomLocalizationKey forKeyPath:LNTXTestTextKeyPath];
    XCTAssertEqual([self.object localizationKeyForKeyPath:LNTXTestTextKeyPath],
                   kRandomLocalizationKey, @"Localization key should be settable and return the same value set previously.");
}

- (void)testObjectThrowsExceptionWhenNilKeyPathIsPassedAsKeyPath {
    XCTAssertThrows([self.object setLocalizationKey:kRandomLocalizationKey forKeyPath:nil],
                    @"Localized object should not accept a nil key path.");
}

- (void)testObjectIsNotLocalizedAnymoreWhenNilIsPassedAsLocalizationKey {
    [self.object setLocalizationKey:kRandomLocalizationKey forKeyPath:LNTXTestTextKeyPath];
    [self.object setLocalizationKey:nil forKeyPath:LNTXTestTextKeyPath];
    XCTAssertNil([self.object localizationKeyForKeyPath:LNTXTestTextKeyPath], @"Localized object should not be localized anymore when nil is passed as localization key");
}

#pragma mark - Localized text

- (void)testLocalizedTextIsEqualToLocalizedValue {
    [self.object setLocalizationKey:kLocalizationKey forKeyPath:LNTXTestTextKeyPath];
    NSString *localizedValue = LNTXLocalizedString(kLocalizationKey);
    
    XCTAssertEqualObjects(self.object.lntx_text, localizedValue,
                          @"Localized text (%@) should be equal to the localized value (%@).",
                          self.object.lntx_text, localizedValue);
}

- (void)testLocalizedTextisEqualToLocalizedValueInTable {
    self.object.localizationTable = kLocalizationTable;
    [self.object setLocalizationKey:kLocalizationKey forKeyPath:LNTXTestTextKeyPath];
    NSString *localizedValue = LNTXLocalizedStringFromTable(kLocalizationKey, kLocalizationTable);
    
    XCTAssertEqualObjects(self.object.lntx_text, localizedValue,
                          @"Localized text (%@) should be equal to the localized value (%@) from table (%@).",
                          self.object.lntx_text, localizedValue, self.object.localizationTable);
}

- (void)testLocalizedTextIsUpdatedWhenLanguageIsUpdated {
    LNTXSetCurrentLanguage(@"en");
    [self.object setLocalizationKey:kLocalizationKey forKeyPath:LNTXTestTextKeyPath];
    LNTXSetCurrentLanguage(@"fr");
    NSString *frenchLocalizedValue = LNTXLocalizedString(kLocalizationKey);
    
    XCTAssertEqualObjects(self.object.lntx_text, frenchLocalizedValue,
                          @"Localized text (%@) should be updated (%@) when the language is updated (%@).",
                          self.object.lntx_text, frenchLocalizedValue, LNTXGetCurrentLanguage());
}

- (void)testLocalizedTextFromTableIsUpdatedWhenLanguageIsUpdated {
    self.object.localizationTable = kLocalizationTable;
    
    LNTXSetCurrentLanguage(@"en");
    [self.object setLocalizationKey:kLocalizationKey forKeyPath:LNTXTestTextKeyPath];
    LNTXSetCurrentLanguage(@"fr");
    NSString *frenchLocalizedValue = LNTXLocalizedStringFromTable(kLocalizationKey, kLocalizationTable);
    
    XCTAssertEqualObjects(self.object.lntx_text, frenchLocalizedValue,
                          @"Localized text (%@) should be updated (%@) when the language is updated (%@).",
                          self.object.lntx_text, frenchLocalizedValue, LNTXGetCurrentLanguage());
}

@end
