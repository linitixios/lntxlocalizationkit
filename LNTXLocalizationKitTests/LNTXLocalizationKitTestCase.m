//
//  LNTXLocalizationKitTestCase.m
//  LNTXLocalizationKit
//
//  Created by Damien Rambout on 17/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import "LNTXLocalizationKitTestCase.h"
#import <XCTest/XCTest.h>
#import "NSBundle+Tests.h"

NSString * const LNTXLocalizationKey = @"some_localization_key";
NSString * const LNTXLocalizationTable = @"SomeLocalizationTable";

@implementation LNTXLocalizationKitTestCase

+ (void)setUp {
    // Swizzle to get a correct main bundle in test environment
    [NSBundle lntx_switchBundleMethods];
    [LNTXLocalizationManagerMock switchSharedManagerMethods];
}

+ (void)tearDown {
    [NSBundle lntx_switchBundleMethods];
    [LNTXLocalizationManagerMock switchSharedManagerMethods];
}

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

@end
