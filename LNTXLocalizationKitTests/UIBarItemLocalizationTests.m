//
//  UIBarItemLocalizationTests.m
//  LNTXLocalizationKit
//
//  Created by Damien Rambout on 17/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "LNTXLocalizationKitTestCase.h"
#import "UIBarItem+LNTXLocalization.h"

@interface UIBarItemLocalizationTests : LNTXLocalizationKitTestCase

@property (nonatomic) UIBarItem *barItem;

@end

@implementation UIBarItemLocalizationTests

- (void)setUp
{
    [super setUp];
    
    self.barItem = [[UIBarButtonItem alloc] init];
}

- (void)tearDown
{
    self.barItem = nil;
    
    [super tearDown];
}

#pragma mark - Localization key

- (void)testLocalizationKeyIsSettable {
    self.barItem.localizationKey = LNTXLocalizationKey;
    
    XCTAssertEqualObjects(self.barItem.localizationKey, LNTXLocalizationKey,
                          @"Localization key should be settable and return the same value set previously.");
}

- (void)testLocalizedTitleIsEqualToLocalizedValue {
    self.barItem.localizationKey = LNTXLocalizationKey;
    NSString *localizedValue = LNTXLocalizedString(LNTXLocalizationKey);
    NSString *text = [self.barItem title];
    
    XCTAssertEqualObjects(text, localizedValue,
                          @"Localized text (%@) should be equal to the localized value (%@).",
                          text, localizedValue);
}

- (void)testLocalizedTitleIsEqualToLocalizedValueFromTable {
    self.barItem.localizationTable = LNTXLocalizationTable;
    self.barItem.localizationKey = LNTXLocalizationKey;
    NSString *localizedValue = LNTXLocalizedStringFromTable(LNTXLocalizationKey, LNTXLocalizationTable);
    NSString *text = [self.barItem title];
    
    XCTAssertEqualObjects(text, localizedValue,
                          @"Localized text (%@) should be equal to the localized value (%@).",
                          text, localizedValue);
}

@end
