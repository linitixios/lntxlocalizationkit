//
//  UIViewControllerLocalizationTests.m
//  LNTXLocalizationKit
//
//  Created by Damien Rambout on 17/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "UIViewController+LNTXLocalization.h"
#import "LNTXLocalizationManagerMock.h"
#import "NSBundle+Tests.h"

static NSString * const kLocalizationKey = @"some_localization_key";
static NSString * const kLocalizationTable = @"SomeLocalizationTable";

@interface UIViewControllerLocalizationTests : XCTestCase

@property (nonatomic) UIViewController *viewController;

@end

@implementation UIViewControllerLocalizationTests

+ (void)setUp {
    // Swizzle to get a correct main bundle in test environment
    [NSBundle lntx_switchBundleMethods];
    [LNTXLocalizationManagerMock switchSharedManagerMethods];
}

+ (void)tearDown {
    [NSBundle lntx_switchBundleMethods];
    [LNTXLocalizationManagerMock switchSharedManagerMethods];
}

- (void)setUp
{
    [super setUp];
    
    self.viewController = [[UIViewController alloc] init];
}

- (void)tearDown
{
    self.viewController = nil;
    
    [super tearDown];
}

#pragma mark - Localization key

- (void)testLocalizationKeyIsSettable {
    self.viewController.localizationKey = kLocalizationKey;
    
    XCTAssertEqualObjects(self.viewController.localizationKey, kLocalizationKey,
                          @"Localization key should be settable and return the same value set previously.");
}

- (void)testLocalizedTitleIsEqualToLocalizedValue {
    self.viewController.localizationKey = kLocalizationKey;
    NSString *localizedValue = LNTXLocalizedString(kLocalizationKey);
    NSString *title = [self.viewController title];
    
    XCTAssertEqualObjects(title, localizedValue,
                          @"Localized text (%@) should be equal to the localized value (%@).",
                          title, localizedValue);
}

- (void)testLocalizedTitleIsEqualToLocalizedValueFromTable {
    self.viewController.localizationTable = kLocalizationTable;
    self.viewController.localizationKey = kLocalizationKey;
    NSString *localizedValue = LNTXLocalizedStringFromTable(kLocalizationKey, kLocalizationTable);
    NSString *title = [self.viewController title];
    
    XCTAssertEqualObjects(title, localizedValue,
                          @"Localized text (%@) should be equal to the localized value (%@).",
                          title, localizedValue);
}

@end
