//
//  UISegmentedControlLocalizationTests.m
//  LNTXLocalizationKit
//
//  Created by Damien Rambout on 17/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "LNTXLocalizationKitTestCase.h"
#import "UISegmentedControl+LNTXLocalization.h"

static NSString *kLocalizationPrefix = @"segmented_control_";

@interface UISegmentedControlLocalizationTests : LNTXLocalizationKitTestCase

@property (nonatomic) UISegmentedControl *control;

@end

@implementation UISegmentedControlLocalizationTests

- (void)setUp
{
    [super setUp];
    
    self.control = [[UISegmentedControl alloc] init];
}

- (void)tearDown
{
    self.control = nil;
    
    [super tearDown];
}

#pragma mark -

#pragma mark - Localization key

- (void)testLocalizationKeyIsSettable {
    self.control.localizationKeyPrefix = kLocalizationPrefix;
    
    XCTAssertEqualObjects(self.control.localizationKeyPrefix, kLocalizationPrefix,
                          @"Localization key prefix should be settable and return the same value set previously.");
}

- (void)testIndexedLocalizationKeyIsNilWhenNoPrefixIsSet {
    self.control.localizationKeyPrefix = nil;
    [self.control insertSegmentWithTitle:@"" atIndex:0 animated:NO];
    
    XCTAssertNil([self.control titleLocalizationKeyForSegmentAtIndex:0],
                 @"Indexed localization key should be nil when the localization prefix is nil.");
}

- (void)testIndexedLocalizationKeyIsValid {
    self.control.localizationKeyPrefix = kLocalizationPrefix;
    [self.control insertSegmentWithTitle:@"" atIndex:0 animated:NO];
    NSString *localizationKey = [NSString stringWithFormat:@"%@0", kLocalizationPrefix];
    NSString *controlLocalizationKey = [self.control titleLocalizationKeyForSegmentAtIndex:0];
    
    XCTAssertEqualObjects(localizationKey, controlLocalizationKey,
                          @"Indexed localization key should (%@) should be equal to the localization key (%@).",
                          controlLocalizationKey, localizationKey);
}

#pragma mark - Localized titles

- (void)testLocalizedTitleIsEqualToLocalizedValue {
    self.control.localizationKeyPrefix = kLocalizationPrefix;
    [self.control insertSegmentWithTitle:@"" atIndex:0 animated:NO];
    NSString *localizedValue = LNTXLocalizedString([self.control titleLocalizationKeyForSegmentAtIndex:0]);
    NSString *title = [self.control titleForSegmentAtIndex:0];
    
    XCTAssertEqualObjects(title, localizedValue,
                          @"Localized title (%@) should be equal to the localized value (%@).",
                          title, localizedValue);
}

- (void)testLocalizedTitleIsEqualToLocalizedValueFromTable {
    self.control.localizationTable = LNTXLocalizationTable;
    self.control.localizationKeyPrefix = kLocalizationPrefix;
    [self.control insertSegmentWithTitle:@"" atIndex:0 animated:NO];
    NSString *localizedValue = LNTXLocalizedStringFromTable([self.control titleLocalizationKeyForSegmentAtIndex:0],
                                                            LNTXLocalizationTable);
    NSString *title = [self.control titleForSegmentAtIndex:0];
    
    XCTAssertEqualObjects(title, localizedValue,
                          @"Localized title (%@) should be equal to the localized value (%@).",
                          title, localizedValue);
}

- (void)testLocalizedTitlesAreValidAndDifferent {
    self.control.localizationKeyPrefix = kLocalizationPrefix;
    [self.control insertSegmentWithTitle:@"" atIndex:0 animated:NO];
    [self.control insertSegmentWithTitle:@"" atIndex:1 animated:NO];
    NSString *localizedValue0 = LNTXLocalizedString([self.control titleLocalizationKeyForSegmentAtIndex:0]);
    NSString *localizedValue1 = LNTXLocalizedString([self.control titleLocalizationKeyForSegmentAtIndex:1]);
    NSString *title0 = [self.control titleForSegmentAtIndex:0];
    NSString *title1 = [self.control titleForSegmentAtIndex:1];
    
    XCTAssertEqualObjects(title0, localizedValue0,
                          @"Localized title at index 0 (%@) should be equal to the localized value 0 (%@).",
                          title0, localizedValue0);
    
    XCTAssertEqualObjects(title1, localizedValue1,
                          @"Localized title at index 1 (%@) should be equal to the localized value 1 (%@).",
                          title1, localizedValue1);
    
    XCTAssertNotEqualObjects(title0, title1,
                             @"Localized title 0 (%@) should be different then title 1 (%@).",
                             title0, title1);
}

@end
