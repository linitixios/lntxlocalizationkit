//
//  NSBundle+Tests.h
//  LNTXLocalizationKit
//
//  Created by Damien Rambout on 04/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSBundle (Tests)

+ (NSBundle *)lntx_testBundle;

+ (void)lntx_switchBundleMethods;

@end
