//
//  NSBundle+Tests.m
//  LNTXLocalizationKit
//
//  Created by Damien Rambout on 04/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import "NSBundle+Tests.h"

#import "LNTXLocalizationKit.h"

#import <objc/runtime.h>

@implementation NSBundle (Tests)

+ (NSBundle *)lntx_testBundle {
    return [NSBundle bundleForClass:[LNTXLocalizationManager class]];
}

+ (void)lntx_switchBundleMethods {
    Method original = class_getClassMethod(self, @selector(mainBundle));
    Method swizzled = class_getClassMethod(self, @selector(lntx_testBundle));
    method_exchangeImplementations(original, swizzled);
}

@end
