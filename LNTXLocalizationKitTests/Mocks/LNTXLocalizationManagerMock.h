//
//  LNTXLocalizationManagerMock.h
//  LNTXLocalizationKit
//
//  Created by Damien Rambout on 04/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import "LNTXLocalizationManager.h"

@interface LNTXLocalizationManagerMock : LNTXLocalizationManager

+ (void)switchSharedManagerMethods;
+ (void)resetSharedInstance;

@end
