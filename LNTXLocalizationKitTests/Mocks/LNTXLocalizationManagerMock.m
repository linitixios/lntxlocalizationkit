//
//  LNTXLocalizationManagerMock.m
//  LNTXLocalizationKit
//
//  Created by Damien Rambout on 04/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import "LNTXLocalizationManagerMock.h"

#import <objc/runtime.h>

@implementation LNTXLocalizationManagerMock

static id sharedManager = nil;

+ (instancetype)lntx_sharedManager {
    if (!sharedManager) {
        sharedManager = [[LNTXLocalizationManagerMock alloc] init];
    }
    
    return sharedManager;
}

+ (void)switchSharedManagerMethods {
    Method original = class_getClassMethod(self, @selector(sharedManager));
    Method swizzled = class_getClassMethod(self, @selector(lntx_sharedManager));
    method_exchangeImplementations(original, swizzled);
}

+ (void)resetSharedInstance {
    sharedManager = nil;
}

@end
