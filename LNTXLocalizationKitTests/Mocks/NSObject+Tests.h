//
//  NSObject+Tests.h
//  LNTXLocalizationKit
//
//  Created by Damien Rambout on 05/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const LNTXTestTextKeyPath;

@interface NSObject (Tests)

@property (nonatomic, copy) NSString *lntx_text;

@end
