//
//  NSObject+Tests.m
//  LNTXLocalizationKit
//
//  Created by Damien Rambout on 05/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import "NSObject+Tests.h"

#import "NSObject+LNTXLocalization.h"
#import <objc/runtime.h>

static void *kTextKey = (__bridge void * const)@"lntx_textKey";

NSString * const LNTXTestTextKeyPath = @"lntx_text";

@implementation NSObject (Tests)

- (void)setLntx_text:(NSString *)lntx_text {
    objc_setAssociatedObject(self, kTextKey,
                             lntx_text, OBJC_ASSOCIATION_COPY);
}

- (NSString *)lntx_text {
    return objc_getAssociatedObject(self, kTextKey);
}

@end
