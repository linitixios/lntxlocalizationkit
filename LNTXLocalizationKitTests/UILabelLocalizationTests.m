//
//  UILabelLocalizationTests.m
//  LNTXLocalizationKit
//
//  Created by Damien Rambout on 05/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "UILabel+LNTXLocalization.h"
#import "LNTXLocalizationManagerMock.h"
#import "NSBundle+Tests.h"

static NSString * const kLocalizationKey = @"some_localization_key";
static NSString * const kLocalizationTable = @"SomeLocalizationTable";

@interface UILabelLocalizationTests : XCTestCase

@property (nonatomic) UILabel *label;

@end

@implementation UILabelLocalizationTests

+ (void)setUp {
    // Swizzle to get a correct main bundle in test environment
    [NSBundle lntx_switchBundleMethods];
    // Swizzle to get a correct language manager
    [LNTXLocalizationManagerMock switchSharedManagerMethods];
}

+ (void)tearDown {
    [NSBundle lntx_switchBundleMethods];
    [LNTXLocalizationManagerMock switchSharedManagerMethods];
}

- (void)setUp
{
    [super setUp];

    self.label = [[UILabel alloc] init];
}

- (void)tearDown
{
    self.label = nil;
    [LNTXLocalizationManagerMock resetSharedInstance];
    
    [super tearDown];
}

#pragma mark - Localization key

- (void)testLocalizationKeyIsSettable {
    self.label.localizationKey = kLocalizationKey;
    
    XCTAssertEqualObjects(self.label.localizationKey, kLocalizationKey,
                          @"Localization key should be settable and return the same value set previously.");
}

- (void)testLocalizedTextIsEqualToLocalizedValue {
    self.label.localizationKey = kLocalizationKey;
    NSString *localizedValue = LNTXLocalizedString(kLocalizationKey);
    
    XCTAssertEqualObjects(self.label.text, localizedValue,
                          @"Localized text (%@) should be equal to the localized value (%@).",
                          self.label.text, localizedValue);
}

- (void)testLocalizedTextIsEqualToLocalizedValueFromTable {
    self.label.localizationTable = kLocalizationTable;
    self.label.localizationKey = kLocalizationKey;
    NSString *localizedValue = LNTXLocalizedStringFromTable(kLocalizationKey, kLocalizationTable);
    
    XCTAssertEqualObjects(self.label.text, localizedValue,
                          @"Localized text (%@) should be equal to the localized value (%@).",
                          self.label.text, localizedValue);
}

@end
