//
//  LNTXLocalizationKitTestCase.h
//  LNTXLocalizationKit
//
//  Created by Damien Rambout on 17/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "LNTXLocalizationManagerMock.h"

extern NSString * const LNTXLocalizationKey;
extern NSString * const LNTXLocalizationTable;

@interface LNTXLocalizationKitTestCase : XCTestCase

@end
