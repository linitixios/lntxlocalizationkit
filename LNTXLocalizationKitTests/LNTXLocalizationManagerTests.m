//
//  LNTXLocalizationManagerTests.m
//  LNTXLocalizationKit
//
//  Created by Damien Rambout on 04/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "LNTXLocalizationManagerMock.h"
#import "NSBundle+Tests.h"

static NSString * const kEnglishLanguageCode = @"en";
static NSString * const kFrenchLanguageCode = @"fr";

static NSString * const kUndefinedLocalizationKey = @"undefined_key";
static NSString * const kLocalizationKey = @"some_localization_key";
static NSString * const kLocalizationTable = @"SomeLocalizationTable";

@interface LNTXLocalizationManagerTests : XCTestCase

@property (nonatomic) NSNotification *willUpdateLanguageNotification;
@property (nonatomic) NSNotification *didUpdateLanguageNotification;

@end

@implementation LNTXLocalizationManagerTests

+ (void)setUp {
    // Swizzle to get a correct main bundle in test environment
    [NSBundle lntx_switchBundleMethods];
    // Swizzle to get a correct language manager
    [LNTXLocalizationManagerMock switchSharedManagerMethods];
}

+ (void)tearDown {
    [NSBundle lntx_switchBundleMethods];
    [LNTXLocalizationManagerMock switchSharedManagerMethods];
}

- (void)setUp
{
    [super setUp];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(languageWillUpdateNotification:)
                                                 name:LNTXLanguageWillUpdateNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(languageDidUpdateNotification:)
                                                 name:LNTXLanguageDidUpdateNotification
                                               object:nil];
}

- (void)tearDown
{
    [LNTXLocalizationManagerMock resetSharedInstance];
    
    self.willUpdateLanguageNotification = nil;
    self.didUpdateLanguageNotification = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:nil];
    
    [super tearDown];
}

- (NSBundle *)bundleForLanguage:(NSString *)language {
    NSString *bundlePath = [[NSBundle mainBundle] pathForResource:language
                                                           ofType:@"lproj"];
    return [NSBundle bundleWithPath:bundlePath];
}

#pragma mark - Notifications

- (void)languageWillUpdateNotification:(NSNotification *)notification {
    self.willUpdateLanguageNotification = notification;
}

- (void)languageDidUpdateNotification:(NSNotification *)notification {
    self.didUpdateLanguageNotification = notification;
}

#pragma mark - Singleton

- (void)testSharedManagerIsNotNil {
    XCTAssertNotNil([LNTXLocalizationManager sharedManager], @"Shared manager should not be nil.");
}

- (void)testSharedManagerIsAlwaysTheSame {
    XCTAssertEqualObjects([LNTXLocalizationManager sharedManager],
                          [LNTXLocalizationManager sharedManager],
                          @"Shared manager should always be the same.");
}

#pragma mark - Current language

- (void)testCurrentLanguageIsPreferredLanguageByDefault {
    XCTAssertEqualObjects([LNTXLocalizationManagerMock sharedManager].currentLanguage,
                   [LNTXLocalizationManagerMock preferredLanguage],
                   @"Default current language (%@) should be the preferred language (%@).",
                   [LNTXLocalizationManagerMock sharedManager].currentLanguage,
                   [LNTXLocalizationManagerMock preferredLanguage]);
}

- (void)testSettingCurrentLanguageToNilResetsItToPreferredLanguage {
    [LNTXLocalizationManagerMock sharedManager].currentLanguage = kFrenchLanguageCode;
    [LNTXLocalizationManagerMock sharedManager].currentLanguage = nil;
    XCTAssertEqualObjects([LNTXLocalizationManagerMock sharedManager].currentLanguage,
                          [LNTXLocalizationManagerMock preferredLanguage],
                          @"Default current language should be the preferred language.");
}

- (void)testCurrentLanguageIsSettable {
    [LNTXLocalizationManagerMock sharedManager].currentLanguage = kFrenchLanguageCode;
    XCTAssertEqualObjects([LNTXLocalizationManagerMock sharedManager].currentLanguage,
                          kFrenchLanguageCode,
                          @"Current language should be settable and return the same value set previously (%@ instead of %@).",
                          [LNTXLocalizationManagerMock sharedManager].currentLanguage, kFrenchLanguageCode);
}

#pragma mark - Notifications

- (void)testUpdateLanguageTriggersNotifications {
    [LNTXLocalizationManagerMock sharedManager].currentLanguage = kFrenchLanguageCode;
    [LNTXLocalizationManagerMock sharedManager].currentLanguage = kEnglishLanguageCode;
    
    XCTAssertNotNil(self.willUpdateLanguageNotification,
                    @"Will update language notification should be triggered when updated language.");
    XCTAssertNotNil(self.didUpdateLanguageNotification,
                    @"Did update language notification should be triggered when updated language.");
}

- (void)testWillUpdateLanguageNotificationContainsPreviousAndCurrentLanguage {
    [LNTXLocalizationManagerMock sharedManager].currentLanguage = kFrenchLanguageCode;
    [LNTXLocalizationManagerMock sharedManager].currentLanguage = kEnglishLanguageCode;
    
    XCTAssertEqualObjects(self.willUpdateLanguageNotification.userInfo[LNTXPreviousLanguageUserInfoKey],
                          kFrenchLanguageCode, @"Previous language should be provided and valid with WillUpdateLanguageNotifications.");
    XCTAssertEqualObjects(self.willUpdateLanguageNotification.userInfo[LNTXCurrentLanguageUserInfoKey],
                          kEnglishLanguageCode, @"Current language should be provided and valid with WillUpdateLanguageNotifications.");
}

- (void)testDidUpdateLanguageNotificationContainsPreviousAndCurrentLanguage {
    [LNTXLocalizationManagerMock sharedManager].currentLanguage = kFrenchLanguageCode;
    [LNTXLocalizationManagerMock sharedManager].currentLanguage = kEnglishLanguageCode;
    
    XCTAssertEqualObjects(self.didUpdateLanguageNotification.userInfo[LNTXPreviousLanguageUserInfoKey],
                          kFrenchLanguageCode, @"Previous language should be provided and valid with DidUpdateLanguageNotifications.");
    XCTAssertEqualObjects(self.didUpdateLanguageNotification.userInfo[LNTXCurrentLanguageUserInfoKey],
                          kEnglishLanguageCode, @"Current language should be provided and valid with DidUpdateLanguageNotifications.");
}

#pragma mark - Current bundle

- (void)testCurrentBundleIsRelatedToPreferredLanguageByDefault {
    [LNTXLocalizationManagerMock sharedManager].currentLanguage = nil;
    NSBundle *preferredBundle = [self bundleForLanguage:[LNTXLocalizationManagerMock sharedManager].currentLanguage];
    XCTAssertEqualObjects([LNTXLocalizationManagerMock sharedManager].currentBundle,
                   preferredBundle,
                   @"Current bundle should be related to preferred language by default");
}

- (void)testCurrentBundleIsUpdatedWhenCurrentLanguageIsUpdated {
    [LNTXLocalizationManagerMock sharedManager].currentLanguage = kEnglishLanguageCode;
    NSBundle *englishBundle = [LNTXLocalizationManagerMock sharedManager].currentBundle;
    
    [LNTXLocalizationManagerMock sharedManager].currentLanguage = kFrenchLanguageCode;
    NSBundle *frenchBundle = [LNTXLocalizationManagerMock sharedManager].currentBundle;
    
    XCTAssertNotEqualObjects(englishBundle, frenchBundle, @"Current bundle should not be the same when the language is updated.");
}

#pragma mark - Localized values

- (void)testLocalizedStringForUndefinedKeyReturnsNil {
    XCTAssertEqualObjects([[LNTXLocalizationManager sharedManager] localizedStringForKey:kUndefinedLocalizationKey], kUndefinedLocalizationKey,
                 @"The localization key should be returned when asking for an undefined localized string.");
}

- (void)testLocalizedStringForValidKeyReturnsTheLocalizedValue {
    NSString *localizedValue = [[[LNTXLocalizationManagerMock sharedManager] currentBundle] localizedStringForKey:kLocalizationKey
                                                                                                            value:nil
                                                                                                            table:nil];
    XCTAssertEqualObjects([[LNTXLocalizationManager sharedManager] localizedStringForKey:kLocalizationKey], localizedValue,
                          @"Localized string should be valid when the localized key is valid too.");
}

- (void)testLocalizedStringFromDefaultTableIsReturnedWhenNoTableIsSpecified {
    NSString *defaultLocalizedValue = [[LNTXLocalizationManagerMock sharedManager] localizedStringForKey:kLocalizationKey];
    NSString *nilTableLocalizedValue = [[LNTXLocalizationManagerMock sharedManager] localizedStringForKey:kLocalizationKey fromTable:nil];
    XCTAssertEqualObjects(defaultLocalizedValue, nilTableLocalizedValue,
                   @"Localized string from default table should be the same as if no table is specified.");
}

- (void)testLocalizedStringFromTableForUndefinedKeyReturnsNil {
    XCTAssertEqualObjects([[LNTXLocalizationManager sharedManager] localizedStringForKey:kUndefinedLocalizationKey
                                                                               fromTable:kLocalizationTable], kUndefinedLocalizationKey,
                 @"The localization key should be returned when asking for an undefined localized string.");
}

- (void)testLocalizedStringFromTableForValidKeyReturnsTheLocalizedValue {
    NSString *localizedValue = [[[LNTXLocalizationManagerMock sharedManager] currentBundle] localizedStringForKey:kLocalizationKey
                                                                                                            value:nil
                                                                                                            table:kLocalizationTable];
    XCTAssertEqualObjects([[LNTXLocalizationManager sharedManager] localizedStringForKey:kLocalizationKey fromTable:kLocalizationTable], localizedValue,
                          @"Localized string should be valid when the localized key is valid too.");
}

#pragma mark - Preferred language

- (void)testPreferredLanguageIsNotNil {
    XCTAssertNotNil([LNTXLocalizationManager preferredLanguage],
                    @"Preferred language should never be nil");
}

#pragma mark - Supported languages

- (void)testThereIsAtLeastOneSupportedLanguage {
    XCTAssertTrue([[LNTXLocalizationManager supportedLanguages] count] > 0,
                  @"There should be at least one supported language.");
}

- (void)testPreferredLanguageIsAlsoASupportedLanguage {
    NSString *preferredLanguage = [LNTXLocalizationManager preferredLanguage];
    XCTAssertTrue([[LNTXLocalizationManager supportedLanguages] containsObject:preferredLanguage],
                  @"There should be at least one supported language.");
}

- (void)testSettingCurrentLanguageThrowsExceptionIfNotSupported {
    // Should not support italian
    XCTAssertThrows([LNTXLocalizationManagerMock sharedManager].currentLanguage = @"it",
                    @"Setting current language should throw an exceptoin if not supported.");
}

@end
